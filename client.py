#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import socket


def main():
    valid_method = ['INVITE', 'ACK', 'BYE']
    try:
        if sys.argv[1] in valid_method:
            method = sys.argv[1]
        else:
            sys.exit('Usage: valid <method>')

        receiver = sys.argv[2].split('@')[0]    # identificador de usuario del receptor
        ip = sys.argv[2].split('@')[1].split(':')[0]    # direccion ip del servidor
        sip_port = int(sys.argv[2].split('@')[1].split(':')[1])    # puerto del servidor

    except ValueError:
        sys.exit('Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>')

    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            if method == 'INVITE':
                head = f'{method} sip:{receiver}@{ip} SIP/2.0'
                body = f'v=0\no=robin@gotham.com 127.0.0.1\ns=misesion\nt=0\nm=audio 34543 RTP'
                message1 = f'{head}\n\r{body}'
                my_socket.sendto(message1.encode('utf-8'), (ip, sip_port))

            data = my_socket.recv(1024) # lo que recibe el cliente del servidor
            if data == 'SIP/2.0 200 OK':
                message2 = f'ACK sip:{receiver}@{ip} SIP/2.0'    # enviamos el ack
                my_socket.sendto(message2.encode('utf-8'), (ip, sip_port))
            print(data.decode('utf-8'))
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
